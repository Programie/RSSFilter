# RSSFilter

A simple proxy allowing to filter RSS feeds.

## How to run

* `docker-compose up`
* Use the frontend available at `http://localhost:8080` to manage your RSS feeds and filters